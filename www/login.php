<?php require('main.php'); 
if(Session::get_data('id') !== null){
	
	header('Location:index.php');
}



?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>

    <link href="lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/new_style.css" rel="stylesheet">
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->

    <!-- ionic/angularjs js -->
    <script src="lib/ionic/js/ionic.bundle.js"></script>

    <!-- cordova script (this will be a 404 during development) -->
    <script src="cordova.js"></script>

    <!-- your app's js -->
    <script src="js/app.js"></script>
  </head>

<body ng-app="medpay"  dir="rtl">
<!-- Center content -->
<ion-view view-title="Login" name="login-view">
<div id="form">
  <ion-content class="padding padding-top">
      <p align="center">
      	 <img class="logo" src="img/logo.png"> <br>
      באפליקציה זו ניתן לרכוש ביטוח עד לסכום של 500 דולר ולקבל החזר מתאים בטרם הסתיימה הנסיעה, כמו כן, ניתן לאתר רופאים בסביבתך ואף לרכוש ביטוח לנסיעתך הבאה
    </p>
 <form ng-submit="createTask(task)" action="login_submit.php" method="post">
        <div class="list">
          <label class="item item-input">
            <input type="text" name="id" placeholder="ת״ז" ng-model="task.id">
          </label>
        </div>
       <div class="list">
          <label class="item item-input">
            <input type="text" name="mail" placeholder="מייל" ng-model="task.mail">
          </label>
       </div> 
       <div class="list">
          <label class="item item-input">
            <input type="text" name="phone" placeholder="מספר טלפון" ng-model="task.phone">
          </label>
       </div> 
        <div class="padding">
          <button type="submit" class="button button-block button-positive login-btn">לכניסה</button>
        </div>
      </form>
           <p align="center">
      לתמיכה - info@insurancename.com 
    </p>
  </ion-content>
 </div>
</ion-view>
</body>

</html>
