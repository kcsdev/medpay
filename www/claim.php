<?php
require('main.php'); 
if(Session::get_data('id') === null){
	
	header('Location:login.php');
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>

    <link href="lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/new_style.css" rel="stylesheet">
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->

    <!-- ionic/angularjs js -->
    <script src="lib/ionic/js/ionic.bundle.js"></script>

    <!-- cordova script (this will be a 404 during development) -->
    <script src="cordova.js"></script>

    <!-- your app's js -->
    <script src="js/app.js"></script>
  </head>

<body ng-app="medpay"  dir="rtl">
	<!-- Center content -->
<ion-view view-title="Login" name="login-view">
  <ion-content style="background: #e6ebee">
  <div class="row">
  	<div class="col-20 claim_step_father active" id="active"><div class="claim_step" align="center">1</div>פרטים אישיים</div>
  	<div class="col-20 claim_step_father"><div class="claim_step" align="center">2</div>פרטי תביעה</div>
  	<div class="col-20 claim_step_father"><div class="claim_step" align="center">3</div>פרטי רופא</div>
  	<div class="col-20 claim_step_father"><div class="claim_step" align="center">4</div>קבלת החזר</div>
  		
</div>
<form>
	<!-- step 1  -->
	<div id="step-1">
	<div class="content_form">
	 <div class="list">
		 האם התביעה על שמי:<br>
		<label class="checkbox">
       <input type="checkbox" onclick="check()">
     </label>
    <span class="near_check">כן</span>
     	<label class="checkbox">
       <input type="checkbox" onclick="check()">
     </label>
    <span class="near_check">לא</span>
     </div>
     <div class="list">
          <label class="item item-input">
            <input type="text" name="id" placeholder="ת״ז" ng-model="task.id">
          </label>
        </div>
          <div class="list">
          <label class="item item-input">
            <input type="date" name="birth_date" placeholder="תאריך לידה" ng-model="task.id">
          </label>
        </div>
        <div class="list">
      <label class="item item-input item-select">
    <div class="input-label">
    </div>
    <select>
      <option>מין</option>
      <option>זכר</option>
      <option>נקבה</option>
    </select>
  </label>
   </div>
       <div class="list">
          <label class="item item-input">
            <input type="date" name="date_leave" placeholder="תאריך יציאה מהארץ" ng-model="task.id">
          </label>
        </div>
        <div class="list">
          <label class="item item-input">
            <input type="text" name="policy" placeholder="מספר פוליסה" ng-model="task.id">
          </label>
        </div>
       <div class="list">
          <label class="item item-input">
            <input type="date" name="policy_start" placeholder="תאריך תחילת פוליסה" ng-model="task.id">
          </label>
        </div>
        <div class="list">
          <label class="item item-input">
            <input type="date" name="policy_end" placeholder="תאריך סיום פוליסה" ng-model="task.id">
          </label>
        </div>
	</div>
	<div class="padding">
      <button type="button" class="button button-block button-positive login-btn form-btn" onclick="change_class()">אישור והמשך</button>
     </div>
    </div>
    <!-- end of step 1  -->
    
   	<!-- step 2  -->
	<div id="step-2">
	<div class="content_form">
	<div class="list">
          <label class="item item-input">
            <input type="date" name="date_visit" placeholder="תאריך הביקור אצל הרופא" ng-model="task.id">
          </label>
        </div>
        <div class="list">
		סיבת הפניה:<br>
     <label class="checkbox">
       <input type="checkbox" checked="true"  onclick="check()">
     </label>
     <span class="near_check">מחלה</span>
     <label class="checkbox">
       <input type="checkbox"  onclick="check()">
     </label>
     <span class="near_check">תאונה</span>
      <label class="checkbox">
       <input type="checkbox"  onclick="check()">
     </label>
     <span class="near_check">תאונת דרכים</span>
      <label class="checkbox">
       <input type="checkbox"  onclick="check()">
     </label>
    <span class="near_check">הריון</span>
      <label class="checkbox">
       <input type="checkbox"  onclick="check()">
     </label>
     <span class="near_check">רפואת שיניים</span>
     </div>
      <div class="list">
      	    	תיאור סימני המחלה:
    <label class="item item-input">
    <textarea placeholder="תיאור..." cols="50"></textarea>
  </label>
      	</div>
      <div class="list">
      	    	מתי החלו הסימנים:
    <label class="item item-input">
    <textarea placeholder="תיאור..." cols="50"></textarea>
  </label>
      	</div>
        <div class="list">
		האם מדובר במחלה או בבעיה קיימת:<br>
     <label class="checkbox">
       <input type="checkbox">
     </label>
     <span class="near_check">כן</span>
     <label class="checkbox">
       <input type="checkbox">
     </label>
     <span class="near_check">לא</span>
     </div>
    <div class="list">
      <label class="item item-input item-select">
    <div class="input-label">
    </div>
    <select>
      <option>סוג ההוצאה</option>
      <option>רופא</option>
      <option>תרופות</option>
    </select>
  </label>
   </div>
       <div class="list pay_labels">
          <label class="item item-input pay">
            <input type="text" class="pay" name="pay" placeholder="סכום ששולם" ng-model="task.id">
             </label>
             <label class="item item-input item-select coin">
             <div class="input-label">
             </div>
           <select>
           	<option>מטבע</option>
             <option>דולר</option>
             <option>שקל</option>
        </select>
          </label>
        </div>
       <div class="list">
       	<br><input type="button" value="העלת תמונה">
       </div>
       <div id="extra_pay"></div>
       <div class="list add_pay">
       	<hr><div onclick="add_pay()"><span>+</span>הוצאה נוספת</div><hr>
       </div>
	</div>
	<div class="padding">
      <button type="button" class="button button-block button-positive login-btn form-btn" onclick="change_class()">אישור והמשך</button>
     </div>
    </div>
    <!-- end of step 2  -->
    
     	<!-- step 3  -->
     	<div id="step-3">
	<div class="content_form">
		<h4>פרטי הרופא</h4>
		 <div class="list">
     <label class="checkbox">
       <input type="checkbox" checked="true"  onclick="check()">
     </label>
     <span class="near_check">רופא</span>
     <label class="checkbox">
       <input type="checkbox" onclick="check()">
     </label>
     <span class="near_check">מרפאה</span>
    <label class="checkbox">
       <input type="checkbox" onclick="check()">
     </label>
    <span class="near_check">ביקור בית</span>
     </div>
     
        <div class="list">
          <label class="item item-input">
            <input type="text" name="id" placeholder="שם הרופא/ה" ng-model="task.id">
          </label>
        </div>
     <div class="list">
      <label class="item item-input item-select">
    <div class="input-label">
    </div>
    <select>
      <option>התמחות</option>
      <option>לב</option>
      <option>אף,אוזן, גרון</option>
      <option>כללי</option>
    </select>
  </label>
   </div>
      <div class="list">
          <label class="item item-input">
            <input type="text" name="id" placeholder="טלפון" ng-model="task.id">
          </label>
        </div>
              <div class="list">
          <label class="item item-input">
            <input type="text" name="id" placeholder="פקס(לא חובה)" ng-model="task.id">
          </label>
        </div>
        <div class="list">
          <label class="item item-input">
            <input type="text" name="id" placeholder="כתובת מייל(לא חובה)" ng-model="task.id">
          </label>
        </div>
    <div class="list">
      <label class="item item-input item-select">
    <div class="input-label">
    </div>
    <select>
      <option>שפות</option>
      <option>אנגלית</option>
      <option>עברית</option>
      <option>ספרדית</option>
    </select>
  </label>
   </div>
    <div class="new_doctor_adress">
    	<div class="list">
    	כתובת
    	</div>
    	<div class="list new_doctor_field">
    	  <label class="item item-input address_field">
            <input type="text" name="id" placeholder="מדינה" ng-model="task.id">
          </label>
         </div>
          <div class="list new_doctor_field">
           <label class="item item-input address_field">
            <input type="text" name="id" placeholder="עיר" ng-model="task.id">
          </label>
           </div>
          <div class="list new_doctor_field">
         <label class="item item-input address_field">
            <input type="text" name="id" placeholder="אזור" ng-model="task.id">
          </label>
           </div>
          <div class="list new_doctor_field">
           <label class="item item-input address_field">
            <input type="text" name="id" placeholder="רחוב" ng-model="task.id">
          </label>
           </div>
    </div>
		</div>
			<div class="padding">
      <button type="button" class="button button-block button-positive login-btn form-btn" onclick="change_class()">אישור והמשך</button>
     </div>
		</div>
     	 <!-- end of step 3  -->
     	  <!-- start of step 4  -->
     	       	<div id="step-4">
	<div class="content_form">
					<div class="padding">
      <button type="button" class="button button-block button-positive login-btn form-btn">כרטיס אשראי    ></button>
     </div>
		</div>
		</div>
     	   <!-- end of step 4  -->
</form>
</ion-content>
 
 
<ion-footer-bar align-title="left" class="bar-assertive footer">
  <div class="buttons col-25" onclick="direct('main')">
    <button class="button"><img src="img/home_footer.png"><br>ראשי</button>
  </div>
    <div class="buttons col-25" onclick="direct('doctors')">
    <button class="button"><img src="img/search_doctor_footer.png"><br>איתור רופא</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_approve')">
    <button class="button   activated"><img src="img/note_footer.png"><br>הגשת תביעה</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_status')">
    <button class="button"><img src="img/status_footer.png"><br>סטטוס תביעה</button>
  </div>
</ion-footer-bar>
 </ion-view>
 <script type="text/javascript" src="js/claim.js"></script>
</body>
</html>