<?php
require('main.php'); 
if(Session::get_data('id') === null){
	
	header('Location:login.php');
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>

    <link href="lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/new_style.css" rel="stylesheet">
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->

    <!-- ionic/angularjs js -->
    <script src="lib/ionic/js/ionic.bundle.js"></script>

    <!-- cordova script (this will be a 404 during development) -->
    <script src="cordova.js"></script>

    <!-- your app's js -->
    <script src="js/app.js"></script>
  </head>

<body ng-app="medpay"  dir="rtl">
	<!-- Center content -->
<ion-view view-title="Login" name="login-view">
  <ion-content style="background: white">

<div class="bar bar-header bar-light title_bar">
  <h1 class="title">פרטי רופא</h1>
</div>
<div class="content">

<div class="row no-padding doctor_img_detail">
<div class="doctor_address">
<div class="col">
<h4 id="doctor_name"></h4>
<img class="star_img" src="img/5-stars.png">
</div>
<div class="col navigate_to_doctor ">
<span class="address" id="address"></span>
<a href="#" class="navigate_doctor">נווט ></a>
</div>
</div>
</div>

<div class="row doctor_detail">
<div class="col"><img src="img/ribbon.png">&nbsp;&nbsp;כלי דם</div>
</div>
<div class="row doctor_detail border_top">
<div class="col" id="phone"></div>
</div>
<div class="row doctor_detail border_top">
<div class="col" id="second_phone"></div>
</div>
<div class="row doctor_detail border_top">
<div class="col" id="mail"></div>
</div>
<div class="row doctor_detail border_top">

<div class="col"><img src="img/global.png">&nbsp;&nbsp;
<span id="lenguage">

</span>
</div>
</div>
</div>
 </ion-content>
 
 
<ion-footer-bar align-title="left" class="bar-assertive footer">
  <div class="buttons col-25" onclick="direct('main')">
    <button class="button"><img src="img/home_footer.png"><br>ראשי</button>
  </div>
    <div class="buttons col-25" onclick="direct('doctors')">
    <button class="button  activated"><img src="img/search_doctor_footer.png"><br>איתור רופא</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_approve')">
    <button class="button"><img src="img/note_footer.png"><br>הגשת תביעה</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_status')">
    <button class="button"><img src="img/status_footer.png"><br>סטטוס תביעה</button>
  </div>
</ion-footer-bar>
 </ion-view>
 <script type="text/javascript" src="js/doctor_details.js"></script>
</body>
</html>