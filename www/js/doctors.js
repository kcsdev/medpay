
function loadJSON(callback) {
  var doctors_output = '';  
  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var doctors = JSON.parse(xhttp.responseText);
      for(i=0; i< doctors.length; i++){
      	 var doctors_output = ''; 
      	 doctors_output += '<div class="doctor">';
      	 doctors_output += '<div class="row">';
      	 doctors_output += '<div class="col">';
      	 doctors_output += '<h3>' + doctors[i].first_name + ' ' + doctors[i].last_name + '</h3>';
      	 doctors_output += '<img class="star_img" src="img/5-stars.png">';
      	 doctors_output += '<p><span class="address"><img src="img/location.png">&nbsp;' + doctors[i].city + ',' + doctors[i].address +'</span></p>';
      	 doctors_output += '<p><span class="address">&nbsp;&nbsp;&nbsp;כלי דם</span></p>';
      	 doctors_output += '<a onclick="choose_doctor('+doctors[i].id +')" class="show_doctor">בחר רופא זה</a>';
      	 doctors_output += '</div>';
      	 doctors_output += '</div>';
      	 doctors_output += '<div class="row">';
      	 doctors_output += '<div class="col navigate">';
      	 doctors_output += '<div class="col-50 right_sub_nav"><img src="img/map.png"> נווט אל רופא זה</div>';
      	 doctors_output += '<div class="col-50 left_sub_nav"><img src="img/phone.png"><a href="'+doctors[i].phone +'">'+doctors[i].phone +'</a></div>';
      	 doctors_output += '</div>';
      	 doctors_output += '</div>';
      	 doctors_output += '</div>'; 
      	 document.getElementById('doctors').innerHTML += doctors_output;
      }
    }
  };
  xhttp.open("GET", "http://innovation-il.co.il/doctors_list.json", true);
  xhttp.send();
 }
 loadJSON();
 
 function choose_doctor(id){
     localStorage.setItem("doctor_id", id);
 	  window.location.href = 'http://localhost/medpay/www/doctor_details.php';
 };

