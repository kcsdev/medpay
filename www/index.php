<?php
require('main.php'); 
if(Session::get_data('id') === null){
	
	header('Location:login.php');
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>

    <link href="lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/new_style.css" rel="stylesheet">
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->

    <!-- ionic/angularjs js -->
    <script src="lib/ionic/js/ionic.bundle.js"></script>

    <!-- cordova script (this will be a 404 during development) -->
    <script src="cordova.js"></script>

    <!-- your app's js -->
    <script src="js/app.js"></script>
  </head>

<body ng-app="medpay"  dir="rtl">
	<!-- Center content -->
<ion-view view-title="Login" name="login-view">
  <ion-content class="padding">
  <div class="main_content">
          <p align="center">
      	 <img class="logo" src="img/logo.png"> <br>
      <span class="action">בחר/י פעולה מבוקשת</span>
    </p> 
<div class="row" align="center">
  <div class="col-50 main_menu">
  <div class="new_menu">
   <a href="doctors.php"><img src="img/search.png"></a>
  </div>
  <p align="center">איתור רופא</p>
  </div>
  <div class="col-50 main_menu">
  	  <div class="new_menu">
     <a href="claim_approve.php"><img src="img/claim.png"></a>
     </div>
    <p  align="center">הגשת תביעה</p>
  </div>
</div>
<div class="row" align="center">
  <div class="col-50 main_menu">
     <div class="new_menu">
    <a href="#"><img src="img/situation.png"></a>
    </div>
    <p  align="center">בירור מצב פניה</p>
  </div>
  <div class="col-50 main_menu">
  	  <div class="new_menu">
   <a href="#"><img src="img/new.png"></a>
   </div>
   <p  align="center">התייעצות רפואית</p>
  </div>
</div>
<!--<div class="doctors_around">
<div class="row" align="center">
<div class="col">
   <p  align="center"><span class="action">רופאים בסביבתך</span></p> 
   <hr>
</div>
</div>
</div>-->
</div>
  </ion-content>

<ion-footer-bar align-title="left" class="bar-assertive footer">
  <div class="buttons col-25" onclick="direct('main')">
    <button class="button activated"><img src="img/home_footer.png"><br>ראשי</button>
  </div>
    <div class="buttons col-25" onclick="direct('doctors')">
    <button class="button"><img src="img/search_doctor_footer.png"><br>איתור רופא</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_approve')">
    <button class="button"><img src="img/note_footer.png"><br>הגשת תביעה</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_status')">
    <button class="button"><img src="img/status_footer.png"><br>סטטוס תביעה</button>
  </div>
</ion-footer-bar>

</ion-view>
</body>

</html>
