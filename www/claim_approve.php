<?php
require('main.php'); 
if(Session::get_data('id') === null){
	
	header('Location:login.php');
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>

    <link href="lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/new_style.css" rel="stylesheet">
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->

    <!-- ionic/angularjs js -->
    <script src="lib/ionic/js/ionic.bundle.js"></script>

    <!-- cordova script (this will be a 404 during development) -->
    <script src="cordova.js"></script>

    <!-- your app's js -->
    <script src="js/app.js"></script>
  </head>

<body ng-app="medpay"  dir="rtl">
	<!-- Center content -->
<ion-view view-title="Login" name="login-view">
  <ion-content>
<<div class="bar bar-header bar-light title_bar">
  <h1 class="title">הגשת תביעה</h1>
</div>
  
          <p align="center">
        אני המבוטח/ת, מאשר/ת בזאת כי קראתי את התקנון המלא של השירות וידוע לי כי אך ורק המבוטח/ת רשאי/ת לקבל החזר בגין הוצאות רפואיות בחו״ל 
        וכי לא ניתן להעביר החזר כזה לאף אדם אחר כמו כן מוצהר בזאת כי בעת הגשת התביעה אין לי כל תביעה כספית נוספת לחברת הביטוח ולא ידוע לי
        על כל צורך בטיפול רפואי נוסף וכי ידוע לי כי כל החזר שיתקבל מתבסס על הצהרתי זו 
    </p>
    <p  align="center" class="white"><a href="#">לקריאת התקנון המלא של השירות</a></p>
     <button type="button" class="button button-block button-positive login-btn approve" onclick="remove_claim()">אישור והמשך</button>
     <p align="center"><small>באפליקציה זו ניתן להגיש בקשה להחזר כספי עד לסכום מקסימלי של 500$</small></p>
</ion-content>
 
 
<ion-footer-bar align-title="left" class="bar-assertive footer">
  <div class="buttons col-25" onclick="direct('main')">
    <button class="button"><img src="img/home_footer.png"><br>ראשי</button>
  </div>
    <div class="buttons col-25" onclick="direct('doctors')">
    <button class="button"><img src="img/search_doctor_footer.png"><br>איתור רופא</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_approve')">
    <button class="button activated"><img src="img/note_footer.png"><br>הגשת תביעה</button>
  </div>
    <div class="buttons col-25" onclick="direct('claim_status')">
    <button class="button"><img src="img/status_footer.png"><br>סטטוס תביעה</button>
  </div>
</ion-footer-bar>
 </ion-view>
 <script type="text/javascript" src="js/approve_claim.js"></script>
</body>
</html>