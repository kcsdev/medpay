<?php

class Security {
  
  private static $pwd;
  
  public static function cleanXss($input, $sanitize ,  $inject = false){
    
    $input = filter_var($input, $sanitize);
    
    if($inject){
      
      $input = self::clean_injection($input);
      
    }
    
    return $input;
    
  }
  
  public static function clean_injection($input){
    
    return mysql_real_escape_string($input);
    
  }
  

}