<?php

class Db {

	protected static $db;
	private $dsn = 'mysql:dbname=ima_medpay;host=localhost;charset=utf8';
    private $user = 'root';
    private $password = '';
	
   //singleton connection
	private function __construct() {

		try {

			Db::$db = new PDO($this->dsn, $this->user, $this->password);
			Db::$db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {

			echo "Connection Error: " . $e -> getMessage();
		}
		

	}
    //connect to db
	public static function getConnection() {

		if (Db::$db === NULL) {
			
			

			new Db();
		}

		return Db::$db;
	}
    //get sql and return results as object
	public static function select($sql) {
	 Db::getConnection();
	 $rows = Db::$db->prepare($sql);
     $rows->setFetchMode(PDO::FETCH_OBJ);
     $rows->execute();
     $results = $rows->fetchAll();
     return $results;
	}
	
	//get associative array and insert into db
	public function insert($table,$arr){
	 Db::getConnection();	
	 $sql = "INSERT INTO " . $table . " SET";
	 foreach($arr as $key=>$value){
	  	$sql .= " " . $key . "=" . $value . ", ";
		
	 }
	 $fff =  substr ($sql,0,strlen($sql)-2);
	  $rows = Db::$db->prepare($fff);
	  $rows->execute();	
	}

}

