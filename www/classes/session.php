<?php
class Session { 

    // Session singleton
    protected static $instance;

    private function __construct()
    {
        //start the session
        session_start();

        Session::$instance = $this; 
    }

    public static function instance()
    {
        if (Session::$instance === NULL)
        {
            new Session;
        }

        return Session::$instance;
    }
	
	public static function save_session($index, $value){
		Session::instance();
	     $_SESSION[$index] = $value;
		
		
	}
	
	public static function get_data($index){
		Session::instance();
		return $_SESSION[$index];
		
	}
}

